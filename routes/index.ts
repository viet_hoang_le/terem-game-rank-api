import {Router} from 'express';

import {Logger} from './services/Logger';

const logger = new Logger('ROUTER_INDEX');

const router_index: Router = Router();

/* GET home page */
router_index.get('/', function (req, res, next) {
  logger.log_c('The Homepage is requested.');
  res.render('index', {title: 'Game Ranking System - Viet'});
});

export {router_index}
