import {BaseModel} from '../models/BaseModel';

import {Logger} from './Logger';

const logger = new Logger('ModelHelper');

class ModelHeper {

  /**
   * setup client-model --> server-model
   * @param sModel
   * @param cModel
   */
  static setupModel(sModel: BaseModel, cModel: any, isDeleteUnsetFields = true): void {
    const updatedAttrs = [];
    // copy only attributes that have values
    const valueAttrKey = Object.keys(sModel.value);
    for (const attribute in cModel.value) {
      if (valueAttrKey.includes(attribute)) {
        sModel.value[attribute] = cModel.value[attribute];
        updatedAttrs.push(attribute);
      }
    }
    if (isDeleteUnsetFields) {
      // remove the attributes which are not updated
      for (const attribute in sModel.value) {
        if (!updatedAttrs.includes(attribute)) {
          delete sModel.value[attribute];
        }
      }
    }
  }
}

export {ModelHeper};
