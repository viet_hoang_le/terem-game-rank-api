import * as debug from 'debug';

export class Logger {

  public log_error: any;
  public log_info: any;
  // log to console only
  public log_c: any;
  public log_c_error: any;
  public log_c_debug: any;
  private log_error_domain = 'b:error';
  private log_info_domain = 'b:info';
  private log_c_domain = 'b:log';
  private log_c_error_domain = 'b:error';
  private log_c_debug_domain = 'b:debug';

  static log_static_c_debug(domain: string, log_message: string) {
    const debugDomain = domain && domain.length > 0 ? 'b:debug_static - ' + domain : 'b:debug_static';
    const log = debug(debugDomain);
    log(log_message);
  }

  constructor(domain: string = null) {
    if (domain) {
      this.log_error_domain = this.log_error_domain + '-' + domain;
      this.log_info_domain = this.log_info_domain + '-' + domain;
      this.log_c_domain = this.log_c_domain + '-' + domain;
      this.log_c_error_domain = this.log_c_error_domain + '-' + domain;
      this.log_c_debug_domain = this.log_c_debug_domain + '-' + domain;
    }
    // writing to debug file
    this.log_error = debug(this.log_error_domain);
    this.log_error.log = console.log.bind(console);

    this.log_info = debug(this.log_info_domain);
    this.log_info.log = console.log.bind(console);

    // writing to the console
    this.log_c = debug(this.log_c_domain);
    this.log_c_error = debug(this.log_c_error_domain);
    this.log_c_debug = debug(this.log_c_debug_domain);
  }
}
