import {Logger} from '../../services/Logger';
import {DbHelper} from '../_modelHelper/DbHelper';

const logger = new Logger('model_Search');

export class Search {

  async findWordsHasPronunciation(keyword: string, offset: number, limit: number): Promise<any> {
    try {
      const queryData = structureQueryFindWordsHasPronunciation(keyword, offset, limit);
      const queryResult = await DbHelper.executeQuery(queryData[0], queryData[1]);
      return queryResult.models;
    } catch (err) {
      logger.log_c_error('findWordsHasPronunciation ERROR = ', err);
      return [];
    }
  }

  async countWordsHasPronunciation(keyword: string): Promise<number> {
    try {
      const queryData = structureQueryFindWordsHasPronunciation(keyword, 0, 0, true);
      const queryResult = await DbHelper.executeQuery(queryData[0], queryData[1], true);
      return queryResult.count_total_unlimited;
    } catch (err) {
      logger.log_c_error('countWordsHasPronunciation ERROR = ', err);
      return 0;
    }
  }

  async findWordsHasNoPronunciation(keyword: string, offset: number, limit: number): Promise<any> {
    try {
      const queryData = structureQueryFindWordsHasNoPronunciation(keyword, offset, limit);
      const queryResult = await DbHelper.executeQuery(queryData[0], queryData[1]);
      return queryResult.models;
    } catch (err) {
      logger.log_c_error('findWordsHasNoPronunciation ERROR = ', err);
      return [];
    }
  }

  async countWordsHasNoPronunciation(keyword: string): Promise<number> {
    try {
      const queryData = structureQueryFindWordsHasNoPronunciation(keyword, 0, 0, true);
      const queryResult = await DbHelper.executeQuery(queryData[0], queryData[1], true);
      return queryResult.count_total_unlimited;
    } catch (err) {
      logger.log_c_error('countWordsHasNoPronunciation ERROR = ', err);
      return 0;
    }
  }

  async findWordsHasNoMeanOrUsage(keyword: string, offset: number, limit: number): Promise<any> {
    try {
      const queryData = structureQueryFindWordsHasNoMeanOrUsage(keyword, offset, limit);
      const queryResult = await DbHelper.executeQuery(queryData[0], queryData[1]);
      return queryResult.models;
    } catch (err) {
      logger.log_c_error('findWordsHasNoMeanOrUsage ERROR = ', err);
      return [];
    }
  }

  async countWordsHasNoMeanOrUsage(keyword: string): Promise<number> {
    try {
      const queryData = structureQueryFindWordsHasNoMeanOrUsage(keyword, 0, 0, true);
      const queryResult = await DbHelper.executeQuery(queryData[0], queryData[1], true);
      return queryResult.count_total_unlimited;
    } catch (err) {
      logger.log_c_error('countWordsHasNoMeanOrUsage ERROR = ', err);
      return 0;
    }
  }

}

const structureQueryFindWordsHasPronunciation = (keyword: string, offset: number, limit: number, isCounting = false): any[] => {
  let query = 'SELECT * ';
  const params = [];
  if (isCounting) {
    query = 'SELECT count(*) '
  }
  query += ' FROM words AS w' +
    ' LEFT JOIN pronunciation AS p' +
    ' ON w.keyid = p.word_keyid' +
    ' WHERE w.status = 4';
  if (!keyword || keyword.replace(/%/g, '').trim().length === 0) {
    query += isCounting ? '' : ' ORDER BY random()';
  }
  else {
    query += ' AND LOWER(w.word) LIKE LOWER($1)';
    const queryKeyword = keyword.endsWith('%') ? keyword : (keyword + '%');
    params.push(queryKeyword);
    query += isCounting ? '' : ' ORDER BY w.word';
  }
  // setup offset limit
  if (limit > 0) {
    query += ` OFFSET $${params.length + 1} LIMIT $${params.length + 2}`;
    params.push(offset.toString());
    params.push(limit.toString());
  }
  return [query, params];
};

const structureQueryFindWordsHasNoPronunciation = (keyword: string, offset: number, limit: number, isCounting = false): any[] => {
  let query = 'SELECT * ';
  const params = [];
  if (isCounting) {
    query = 'SELECT count(*) '
  }
  query += ' FROM words AS w';

  if (!keyword || keyword.replace(/%/g, '').trim().length === 0) {
    const condition = ' WHERE w.status = 4 AND (w.keyid NOT IN (' +
      ' SELECT DISTINCT p.word_keyid FROM pronunciation AS p) )';
    query += (condition + (isCounting ? '' : ' ORDER BY random()'));
  }
  else {
    const condition = ' WHERE w.status = 4 AND LOWER(w.word) LIKE LOWER($1) AND (w.keyid NOT IN (' +
      ' SELECT DISTINCT p.word_keyid FROM pronunciation AS p) )';
    const queryKeyword = keyword.endsWith('%') ? keyword : (keyword + '%');
    params.push(queryKeyword);
    query += (condition + (isCounting ? '' : ' ORDER BY w.word'));
  }
  // setup offset limit
  if (limit > 0) {
    query += ` OFFSET $${params.length + 1} LIMIT $${params.length + 2}`;
    params.push(offset.toString());
    params.push(limit.toString());
  }
  return [query, params];
};

const structureQueryFindWordsHasNoMeanOrUsage = (keyword: string, offset: number, limit: number, isCounting = false): any[] => {
  let query = 'SELECT * ';
  const params = [];
  if (isCounting) {
    query = 'SELECT count(*) '
  }
  query += ' FROM words AS w';

  const unionSelect = 'SELECT DISTINCT u.word_keyid FROM ' +
    '(SELECT DISTINCT m.word_keyid FROM meaning AS m UNION SELECT mu.word_keyid FROM meaning_usage AS mu) ' +
    'AS u';
  if (!keyword || keyword.replace(/%/g, '').trim().length === 0) {
    const condition = ' WHERE w.status = 4 AND (w.keyid NOT IN (' + unionSelect + ') )';
    query += (condition + (isCounting ? '' : ' ORDER BY random()'));
  }
  else {
    const condition = ' WHERE w.status = 4 AND LOWER(w.word) LIKE LOWER($1) AND (w.keyid NOT IN (' +
      ' SELECT DISTINCT p.word_keyid FROM pronunciation AS p) )';
    const queryKeyword = keyword.endsWith('%') ? keyword : (keyword + '%');
    params.push(queryKeyword);
    query += (condition + (isCounting ? '' : ' ORDER BY w.word'));
  }
  // setup offset limit
  if (limit > 0) {
    query += ` OFFSET $${params.length + 1} LIMIT $${params.length + 2}`;
    params.push(offset.toString());
    params.push(limit.toString());
  }
  return [query, params];
};
