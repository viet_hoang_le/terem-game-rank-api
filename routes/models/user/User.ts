import {BaseModel} from '../BaseModel';
import moment = require('moment');
import {DbLimitation} from '../_modelHelper/DbLimitation';
import {DEFAULT_IMG_USER} from '../../../app_config';

export interface User {
  keyid: string;
  value: UserValue;
}
export interface UserValue {
  username: string;
  display_name: string;
  updated_at: number;
  created_at: number;
  img_url: string;
}

const now = () => new Date().getTime();

export class User extends BaseModel implements User {

  value = {
    username: '',
    display_name: '',
    img_url: DEFAULT_IMG_USER,
    updated_at: now(),
    created_at: now(),
  };

  constructor() {
    super('user', 'u', false);
  }

  async findByKeyidOrUsername(keyid?: string, username?: string): Promise<User> {
    const orFilters = {};
    if (keyid && keyid.length > 0) {
      orFilters['keyid'] = keyid;
    }
    if (username && username.length > 0) {
      orFilters['username'] = username;
    }
    const filters = [orFilters];

    try {
      const rawModel = await this.findUniqueRawModel(filters);

      return (rawModel ? new User().setupModel(rawModel) : null);
    } catch (e) {
      return e;
    }
  }

  /**
   * Get models of User
   * @param limitations
   * @param filters
   * @returns {Promise<Game[]>}
   */
  async findModels(limitations: DbLimitation, filters: any): Promise<User[]> {
    try {
      const rawModels = await this.findRawModels(limitations, filters);

      return rawModels.map(rawModel => new User().setupModel(rawModel));
    } catch (e) {
      return e;
    }
  }

  /*findUserBasic(leftFilters: any, limitation = new DbLimitation(), rightFilters?: any): Promise<any[]> {
    const leftModel = this;
    const rightModel = new UserBasicInfo();
    const leftCollection = ['keyid', 'username', 'email', 'role', 'status', 'created_at'];
    const rightCollection = ['displayname', 'avatar_url', 'cover_url', 'home_url', 'quote'];
    const onCondition = leftModel.alias + '.keyid = ' + rightModel.alias + '.keyid';
    return new Promise((resolve, reject) => {
      this.join(leftModel, rightModel,
        leftCollection, rightCollection, onCondition, leftFilters, rightFilters, limitation).then(queryResult => {
        resolve(queryResult.models);
      }).catch(err => reject(err));
    });
  }*/
}
