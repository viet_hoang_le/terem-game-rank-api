import {BaseModel} from '../BaseModel';

export interface UserGameInfo {
  keyid: string;
  value: UserGameInfoValue;
}
export interface RecordInfo {
  [opponent_keyid: string]: {
    total: number;
    win: number;
  };
}
export interface GameInfo {
  [game_keyid: string]: {
    total: number;
    win: number;
    records: RecordInfo;
  };
}
export interface UserGameInfoValue {
  game_info: GameInfo;
}

export class UserGameInfo extends BaseModel implements UserGameInfo {
  value: UserGameInfoValue = {
    game_info: {},
  };

  constructor(user_keyid: string) {
    super('user_game_info', 'uGI');
    this.keyid = user_keyid;
  }

  /**
   * Find by KeyId will return the unique UserGameInfo with given KeyId
   * @param keyid
   */
  async findByKeyid(keyid: string): Promise<UserGameInfo> {
    try {
      const rawModel = await this.findRawByKeyid(keyid);

      return (rawModel ? new UserGameInfo(keyid).setupModel(rawModel) : null);
    } catch (e) {
      return e;
    }
  }

  /*findUserBasic(leftFilters: any, limitation = new DbLimitation(), rightFilters?: any): Promise<any[]> {
    const leftModel = this;
    const rightModel = new UserBasicInfo();
    const leftCollection = ['keyid', 'username', 'email', 'role', 'status', 'created_at'];
    const rightCollection = ['displayname', 'avatar_url', 'cover_url', 'home_url', 'quote'];
    const onCondition = leftModel.alias + '.keyid = ' + rightModel.alias + '.keyid';
    return new Promise((resolve, reject) => {
      this.join(leftModel, rightModel,
        leftCollection, rightCollection, onCondition, leftFilters, rightFilters, limitation).then(queryResult => {
        resolve(queryResult.models);
      }).catch(err => reject(err));
    });
  }*/
}
