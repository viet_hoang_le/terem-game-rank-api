import * as shortid from 'shortid';
import * as uuid from 'uuid';
import * as moment from 'moment';
import {DbHelper, QueryResult} from './_modelHelper/DbHelper';

import {Logger} from '../services/Logger';
import {DbPostgres} from './_modelHelper/DbPostgres';
import {DbLimitation} from './_modelHelper/DbLimitation';

const logger = new Logger('BaseModel');

abstract class BaseModel {

  dbHelper: DbHelper;
  collectionName: string;
  alias: string;
  keyid: string;
  value: any = {};
  // defines fields that only admin/super_admin can update/insert
  protected customQuery = DbHelper.executeQuery;

  static now = (): number => new Date().getTime();

  constructor(collectionName: string, alias: string, isShort_keyid = true) {
    this.collectionName = collectionName || null;
    this.alias = alias || null;
    // 36 chars // uuid.v1() --> Generate a v1 (time-based) id
    this.keyid = isShort_keyid ? shortid.generate() : uuid.v1();

    this.dbHelper = new DbHelper(collectionName, alias);
  }

  setupModel(sqlModel: any) {
    for (const key in sqlModel) {
      // logger.log_c_debug(`key ${key} = ${sqlModel[key]} => ${typeof this.value[key]}`);
      if (key === 'keyid') {
        this.keyid = sqlModel.keyid;
      } else if (Array.isArray(this.value[key])) {
        this.value[key] = DbPostgres.parseArray(sqlModel[key]);
      } else if (typeof this.value[key] === 'object') {
        try {
          this.value[key] = JSON.parse(sqlModel[key]);
        } catch (ex) {
          logger.log_error('parseObject ERROR = ' + JSON.stringify(ex));
          logger.log_error('parseObject ERROR input = ' + JSON.stringify(sqlModel[key]));
          // logger.log_c_error('parseObject ERROR = ' + JSON.stringify(ex));
          // logger.log_c_error('parseObject ERROR input = ' + JSON.stringify(sqlModel[key]));
        }
      }
      else {
        this.value[key] = sqlModel[key];
      }
    }
    return this;
  }

  getKeyValueObj = () => ({keyid: this.keyid, value: this.value});

  /**
   * Query total number of result records
   * @param filters :{status: Status.list.sharing_status.public.code, language: language,...}
   * @returns {*}
   */
  count(filters: any): Promise<number> {
    return new Promise((resolve, reject) => {
      this.dbHelper.findModels(new DbLimitation(), filters, true).then(queryResult => {
        resolve(queryResult.count_total_unlimited);
      }).catch(err => reject(err));
    });
  }

  getField(field: string): string {
    return this.alias + '.' + field
  };

  /**
   * Updating the existing record based on its unique keyid
   * @param isTimeUpdate
   * @returns {Promise<boolean>}
   */
  updateByKeyid(isTimeUpdate = true): Promise<void> {
    if (isTimeUpdate && this.value['updated_at']) {
      this.value['updated_at'] = BaseModel.now().toString();
    }

    return this.dbHelper.updateByKeyid(this);
  }

  /**
   * Insert/create a new record for this model
   * @returns {Promise<boolean>}
   */
  async insert(): Promise<void> {
    return await this.dbHelper.insert(this);
  }

  /**
   * Saving model into database
   * @returns {Promise<boolean>}
   */
  save(isTimeUpdate = true): Promise<QuerySaveResult> {
    const result = new QuerySaveResult();
    return new Promise((resolve, reject) => {
      this.findRawByKeyid(this.keyid).then(foundRawModel => {
        if (foundRawModel) {
          this.updateByKeyid(isTimeUpdate).then(() => {
            resolve(result);
          }).catch(err => {
            reject(new Error('ERROR save - update'));
          });
        }
        else {
          this.insert().then(() => {
            result.action = QuerySaveResult.actions.insert;
            resolve(result);
          }).catch(err => {
            reject(new Error('ERROR save - insert'));
          });
        }
      }).catch(err => {
        logger.log_error('ERROR save = ' + JSON.stringify(err));
        // logger.log_c_error('ERROR save = ' + JSON.stringify(err));
        reject(new Error('ERROR save - ' + err));
      });
    });
  }

  /**
   * Delete by KeyId will delete the BaseModel with given KeyId
   * @returns {Promise<boolean>}
   */
  deleteByKeyId(): Promise<void> {
    return this.dbHelper.deleteByKeyId(this.keyid);
  }

  protected join(leftModel: any, rightModel: any,
                 leftSelectionArr: any[], rightSelectionArr: any[], onCondition: string,
                 leftFilters: any, rightFilters = {}, limitations?: DbLimitation, isLimitationLeft = true,
                 isQueryUnlimitedCount = false): Promise<QueryResult | any> {
    return this.dbHelper.join(leftModel, rightModel, leftSelectionArr, rightSelectionArr, onCondition,
      leftFilters, rightFilters, limitations, isLimitationLeft, isQueryUnlimitedCount);
  }

  protected deleteModels(filters: any): Promise<void> {
    return this.dbHelper.deleteModels(filters);
  }

  protected insertModels(sqlModels: any[], columnArray: string[]): Promise<void> {
    return this.dbHelper.insertModels(sqlModels, columnArray);
  }

  /**
   * If filters is an array of only one element (an object), treat it with setup_OR_condition
   * Default is (key1 LIKE value1 AND key2 LIKE value2)
   * In case value is an array object like key1:[value1.1, value1.2] it means (key1 LIKE value1.1 OR key1 LIKE value1.2)
   * It also means that if we need to form a condition like this (key1 LIKE value0 OR key2 LIKE value0), we can reverse
   * the key:value position like value0:[key1,key2] to form the query (value0 LIKE key1 OR value0 LIKE key2)
   * ==> The final query condition can be in this form: WHEN (key1 LIKE value1 AND key2 LIKE value2
   *                                            AND ( value3 LIKE key3-1 OR value3 LIKE key3-2 )
   * @param limitations
   * @param filters  - map of key & its value {key1:value1, key2:value2}.
   * @returns {Promise<T>}
   */
  protected findRawModels(limitations: DbLimitation, filters: any): Promise<any[]> {
    return new Promise((resolve, reject) => {
      this.dbHelper.findModels(limitations, filters)
        .then(queryResult => {
        resolve(queryResult.models);
      }).catch(err => reject(err));
    });
  }

  /**
   * return a unique object based on filters
   * @param filters
   * @returns {Promise<T>}
   */
  protected findUniqueRawModel(filters: any): Promise<any> {
    return this.dbHelper.findByUniques(filters);
  }

  /**
   * Find by KeyId will return the unique model with given KeyId
   * @returns {Promise<any>}
   */
  protected findRawByKeyid(keyid: string): Promise<any> {
    const filter = {keyid};
    return this.findUniqueRawModel(filter);
  }

  /**
   * Search a number (limit) of random models based on the index key
   * @param {number} limit
   * @param {string} randomByKey
   * @returns {Promise<any[]>}
   */
  protected findRandomRawModels(limit: number, randomByKey = 'keyid', filters: any = null): Promise<any[]> {
    return new Promise((resolve, reject) => {
      this.dbHelper.findRandomModels(limit, randomByKey, filters).then(queryResult => {
        resolve(queryResult.models);
      }).catch(err => reject(err));
    });
  }
}

class QuerySaveResult {
  static actions = {
    insert: 'insert',
    update: 'update',
  };

  action: string;

  constructor() {
    this.action = QuerySaveResult.actions.update;
  }
}

export {BaseModel, QuerySaveResult};
