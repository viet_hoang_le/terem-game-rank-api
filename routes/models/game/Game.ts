import {BaseModel} from '../BaseModel';
import {Logger} from '../../services/Logger';
import moment = require('moment');
import {DbLimitation} from '../_modelHelper/DbLimitation';
import {DEFAULT_IMG_GAME} from '../../../app_config';

export interface Game {
  keyid: string;
  value: GameValue;
}
export interface GameValue {
  code: string;
  name: string;
  updated_at: number;
  created_at: number;
  description: string;
  img_url: string;
}

const logger = new Logger('model_Game');
const now = () => new Date().getTime();

export class Game extends BaseModel implements Game {

  value = {
    code: '',
    name: '',
    description: '',
    img_url: DEFAULT_IMG_GAME,
    updated_at: now(),
    created_at: now(),
  };

  constructor() {
    super('game', 'g');
  }

  /**
   * Find by KeyId will return the unique Game with given KeyId
   * @param keyid
   */
  async findByKeyidOrCode(keyid: string, code?: string): Promise<Game> {
    const orFilters = {};
    if (keyid && keyid.length > 0) {
      orFilters['keyid'] = keyid;
    }
    if (code && code.length > 0) {
      orFilters['code'] = code;
    }
    const filters = [orFilters];

    try {
      const rawModel = await this.findUniqueRawModel(filters);

      return (rawModel ? new Game().setupModel(rawModel) : null);
    } catch (e) {
      return null;
    }
  }

  /**
   * Get models of Game
   * @param limitations
   * @param filters
   * @returns {Promise<Game[]>}
   */
  async findModels(limitations: DbLimitation, filters: any): Promise<Game[]> {
    try {
      const rawModels = await this.findRawModels(limitations, filters);

      return rawModels.map(rawModel => new Game().setupModel(rawModel));
    } catch (e) {
      logger.log_error('findModels ERROR = ', e);
      return e;
    }
  }
}
