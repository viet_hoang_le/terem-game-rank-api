import {BaseModel} from '../BaseModel';
import {Logger} from '../../services/Logger';
import moment = require('moment');
import {DbLimitation} from '../_modelHelper/DbLimitation';

export interface Match {
  keyid: string;
  value: MatchValue;
}
export interface MatchValue {
  game_keyid: string;
  participant: number;
  created_at: number;
}

const logger = new Logger('model_Match');
const now = () => new Date().getTime();

export class Match extends BaseModel implements Match {

  value = {
    game_keyid: '',
    participant: 2,
    created_at: now(),
  };

  constructor() {
    super('match', 'm', false);
  }

  /**
   * Find by KeyId will return the unique Match with given KeyId
   * @param keyid
   */
  async findByKeyid(keyid: string): Promise<Match> {
    try {
      const rawModel = await this.findRawByKeyid(keyid);
      logger.log_c_debug('match_model = ', rawModel);

      return (rawModel ? new Match().setupModel(rawModel) : null);
    } catch (e) {
      return e;
    }
  }

  /**
   * Get models of Match
   * @param limitations
   * @param filters
   * @returns {Promise<Match[]>}
   */
  async findModels(limitations: DbLimitation, filters: any): Promise<Match[]> {
    try {
      const rawModels = await this.findRawModels(limitations, filters);

      return rawModels.map(rawModel => new Match().setupModel(rawModel));
    } catch (e) {
      logger.log_error('findModels ERROR = ', e);
      return e;
    }
  }
}
