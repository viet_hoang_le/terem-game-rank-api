import {BaseModel} from '../BaseModel';
import {Logger} from '../../services/Logger';
import {DbLimitation} from '../_modelHelper/DbLimitation';

export enum MatchRecordStatus {
  WIN = 1,
  LOSS = 0,
}

export interface MatchRecord {
  keyid: string;
  value: MatchRecordValue;
}
export interface MatchRecordValue {
  match_keyid: string;
  user_keyid: string;
  status: MatchRecordStatus;
}

const logger = new Logger('model_Match');

export class MatchRecord extends BaseModel implements MatchRecord {

  value = {
    match_keyid: '',
    user_keyid: '',
    status: MatchRecordStatus.LOSS,
  };

  constructor() {
    super('match_record', 'mR', false);
  }

  /**
   * Find by KeyId will return the unique MatchRecord with given KeyId
   * @param keyid
   */
  async findByKeyid(keyid: string): Promise<MatchRecord> {
    try {
      const rawModel = await this.findRawByKeyid(keyid);

      return (rawModel ? new MatchRecord().setupModel(rawModel) : null);
    } catch (e) {
      return e;
    }
  }

  /**
   * Get models of MatchRecord
   * @param limitations
   * @param filters
   * @returns {Promise<MatchRecord[]>}
   */
  async findModels(limitations: DbLimitation, filters: any): Promise<MatchRecord[]> {
    try {
      const rawModels = await this.findRawModels(limitations, filters);

      return rawModels.map(rawModel => new MatchRecord().setupModel(rawModel));
    } catch (e) {
      logger.log_error('findModels ERROR = ', e);
      return e;
    }
  }
}
