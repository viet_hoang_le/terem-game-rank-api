import {POSTGRESQL} from '../../../app_config';
import {Pool, PoolClient} from 'pg';
import {BaseModel} from '../BaseModel';

import {Logger} from '../../services/Logger';
import {DbLimitation} from './DbLimitation';
import {DbPostgres} from './DbPostgres';

const logger = new Logger('DbHelper');

const pool = new Pool(POSTGRESQL);

class DbHelper {

  private collectionName: string;
  private alias: string;
  private collection: string;

  /**
   * @param queryStatement
   * @param params
   * @param isQueryUnlimitedCount
   * @returns {Promise<QueryResult>}
   */
  static executeQuery(queryStatement: string, params: any[],
                      isQueryUnlimitedCount: boolean = false): Promise<QueryResult> {
    const queryResult = new QueryResult();
    return new Promise((resolve, reject) => {
      pool.connect()
        .then((client: PoolClient) => {
          return client.query(queryStatement, params).then(res => {
            // call done() to release the client back to the pool
            client.release();
            logger.log_c_debug('query = ' + queryStatement);
            logger.log_c_debug('params = ' + JSON.stringify(params));
            if (isQueryUnlimitedCount) {
              queryResult.count_total_unlimited = res.rows[0].count;
            }
            else {
              queryResult.models = res.rows;
            }
            // logger.log_c_debug('query result = ' + JSON.stringify(result));
            resolve(queryResult);
          }).catch((err) => {
            client.release();
            logger.log_error('error running query = ' + JSON.stringify(err));
            logger.log_error('query = ' + queryStatement);
            logger.log_error('params = ' + JSON.stringify(params));
            /*logger.log_c_error(err.stack);
            logger.log_c_error('error running query = ' + JSON.stringify(err));
            logger.log_c_debug('query = ' + queryStatement);
            logger.log_c_debug('params = ' + JSON.stringify(params));*/
            reject(err);
          });
        }).catch(err => {
        logger.log_error('ERROR postgres connection = ' + JSON.stringify(err));
        // logger.log_c_error('ERROR postgres connection = ' + JSON.stringify(err));
        reject(err);
      });
    });
  }

  constructor(collectionName: string, alias: string) {
    this.collectionName = collectionName;
    this.alias = alias;
    this.collection = `"${collectionName}" AS ${alias}`;
  }

  /**
   * Setup model for storing in database
   * @returns {{keyid: string}}
   */
  setupSqlModel(model: BaseModel): any {
    const sqlModel = {
      keyid: model.keyid
    };
    Object.keys(model.value).forEach(key => sqlModel[key] = model.value[key]);
    return sqlModel;
  }

  /**
   * Delete by KeyId will delete the BaseModel with given KeyId
   * @returns {Promise<boolean>}
   */
  deleteByKeyId(keyid: string): Promise<void> {
    const queryStatement = 'DELETE FROM ' + this.collection + ' WHERE keyid = $1';
    const params = [keyid];
    return new Promise<void>((resolve, reject) => {
      DbHelper.executeQuery(queryStatement, params, false).then(queryResult => {
        resolve();
      }).catch(err => {
        logger.log_error('ERROR deleteByKeyId');
        // logger.log_c_error('ERROR deleteByKeyId');
        reject(new Error('ERROR deleteByKeyId'));
      });
    });
  }

  /**
   * Insert/create a new record for this model
   * @returns {Promise<boolean>}
   */
  insert(model: BaseModel): Promise<void> {
    const sqlModel = this.setupSqlModel(model);
    const cols = Object.keys(sqlModel);
    const valueObj: ParamsValue = this.setupObjValueParams(sqlModel, 1);
    const queryStatement = 'INSERT INTO ' + this.collection + '(' + cols + ') VALUES (' + valueObj.valueParams + ') ';
    const params = valueObj.values;
    return new Promise<void>((resolve, reject) => {
      DbHelper.executeQuery(queryStatement, params, false).then(queryResult => {
        resolve();
      }).catch(err => {
        logger.log_error('ERROR insert');
        logger.log_c_error('ERROR insert');
        reject(new Error('ERROR insert'));
      });
    });
  }

  /**
   * Updating the existing record based on its unique keyid
   * @returns {Promise<boolean>}
   */
  updateByKeyid(model: BaseModel): Promise<void> {
    const sqlModel = this.setupSqlModel(model);
    const setCols: string[] = this.setupSetColumnParams(sqlModel, 2);
    const queryStatement = 'UPDATE ' + this.collection + ' SET ' + setCols + ' WHERE keyid = $1';
    let params = [model.keyid];
    params = params.concat(Object.keys(sqlModel).map(function (key) {
      return sqlModel[key]
    }));

    return new Promise<void>((resolve, reject) => {
      DbHelper.executeQuery(queryStatement, params, false).then(queryResult => {
        resolve();
      }).catch(err => {
        logger.log_error('ERROR updateByKeyid');
        // logger.log_c_error('ERROR updateByKeyid');
        reject(new Error('ERROR updateByKeyid'));
      });
    });
  }

  /**
   * return a unique object based on filters
   * @param filters
   * @returns {Promise<T>}
   */
  findByUniques(filters: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.findModels(new DbLimitation(), filters).then(queryResult => {
        if (queryResult.models.length === 1) {
          resolve(queryResult.models[0]);
        } else if (queryResult.models.length === 0) {
          resolve(null);
        }
        else {
          logger.log_error('queryResult is not unique, length = ' + queryResult.models.length);
          // logger.log_c_error('queryResult is not unique, length = ' + queryResult.models.length);
          reject(new Error('queryResult is not unique, length = ' + queryResult.models.length));
        }
      }).catch(err => {
        logger.log_error('ERROR findByUniques');
        // logger.log_c_error('ERROR findByUniques = ' + JSON.stringify(err));
        reject(new Error('ERROR findByUniques'));
      });
    });
  }

  /**
   * If filters is an array of only one element (an object), treat it with setup_OR_condition
   * I.e.: filters = [{ a:value_1, b: value_2, c: value_3 }] ==> condition: a LIKE value_1 OR b LIKE value_2 OR c LIKE value_3
   * Default is (key1 LIKE value1 AND key2 LIKE value2)
   * In case value is an array object like key1:[value1.1, value1.2] it means (key1 LIKE value1.1 OR key1 LIKE value1.2)
   * It also means that if we need to form a condition like this (key1 LIKE value0 OR key2 LIKE value0), we can reverse
   * the key:value position like value0:[key1,key2] to form the query (value0 LIKE key1 OR value0 LIKE key2)
   * ==> The final query condition can be in this form: WHEN (key1 LIKE value1 AND key2 LIKE value2
   *                                            AND ( value3 LIKE key3-1 OR value3 LIKE key3-2 )
   * @param limitations
   * @param filters  - map of key & its value {key1:value1, key2:value2}.
   * @param isQueryUnlimitedCount
   * @returns {Promise<QueryResult>}
   */
  findModels(limitations: DbLimitation, filters: any, isQueryUnlimitedCount = false): Promise<QueryResult> {
    let queryStatement = '';
    let condition = '';
    let params: any = [];
    let conditionData: ConditionData = {
      conditionString: '',
      conditionParams: []
    };
    if (Array.isArray(filters) && filters.length === 1) {  // read description (declare) above to understand how to use it
      conditionData = this.setup_OR_condition(filters[0], this.alias, 1);
      if (conditionData.conditionString.length > 0) {
        condition = ' WHERE ( ' + conditionData.conditionString + ' ) ';
        params = conditionData.conditionParams;
      }
    }
    else {
      conditionData = this.setupConditionMap(filters, this.alias, 1);
      if (conditionData.conditionString.length > 0) {
        condition = ' WHERE ( ' + conditionData.conditionString + ' ) ';
        params = conditionData.conditionParams;
      }
    }

    const order_limit_conditionData: ConditionDataLimit =
      this.setupLimitations(this.alias, limitations, isQueryUnlimitedCount, params.length + 1);
    params = params.concat(order_limit_conditionData.conditionParams);
    if (isQueryUnlimitedCount) {
      queryStatement = `SELECT COUNT(*) AS count FROM "${this.collection}" ${condition}`;
    }
    else {
      queryStatement = 'SELECT * FROM ' + this.collection
        + condition + order_limit_conditionData.orderCondition
        + order_limit_conditionData.limitCondition;
    }
    return new Promise((resolve, reject) => {
      DbHelper.executeQuery(queryStatement, params, isQueryUnlimitedCount).then(queryResult => {
        resolve(queryResult);
      }).catch(err => {
        logger.log_error('ERROR findModels');
        // logger.log_c_error('ERROR findModels = ' + JSON.stringify(err));
        reject(new Error('ERROR findModels'));
      });
    });
  }

  /**
   * @returns {Promise<QueryResult>}
   * @param limit
   * @param randomByKey
   * @param filters
   */
  findRandomModels(limit: number, randomByKey = 'keyid', filters: any = null): Promise<QueryResult> {
    let condition = '';
    let params: any = [];
    let conditionData: ConditionData = {
      conditionString: '',
      conditionParams: []
    };
    if (Array.isArray(filters) && filters.length === 1) {  // read description (declare) above to understand how to use it
      conditionData = this.setup_OR_condition(filters[0], this.alias, 1);
      if (conditionData.conditionString.length > 0) {
        condition = ' WHERE ( ' + conditionData.conditionString + ' ) ';
        params = conditionData.conditionParams;
      }
    }
    else {
      conditionData = this.setupConditionMap(filters, this.alias, 1);
      if (conditionData.conditionString.length > 0) {
        condition = ' WHERE ( ' + conditionData.conditionString + ' ) ';
        params = conditionData.conditionParams;
      }
    }
    const key = `${this.alias}.${randomByKey}`;
    const tempAlias = 'tempT';
    const tempkey = `${tempAlias}.${randomByKey}`;
    const queryStatement = `SELECT * FROM ${this.collection} WHERE ${key} IN ` +
    `(SELECT ${tempkey} FROM (SELECT ${key} FROM "${this.collection}" ${condition} ORDER BY random() LIMIT ${limit}) AS ${tempAlias})`;
    return new Promise((resolve, reject) => {
      DbHelper.executeQuery(queryStatement, params).then(queryResult => {
        resolve(queryResult);
      }).catch(err => {
        logger.log_error('ERROR findModels');
        // logger.log_c_error('ERROR findModels = ' + JSON.stringify(err));
        reject(new Error('ERROR findModels'));
      });
    });
  }

  /**
   * delete models based on input conditions in filters
   * @param filters
   * @returns {Promise<boolean>}
   */
  deleteModels(filters: any): Promise<void> {
    let condition = ''; // setup condition
    let params: any[] = [];
    let conditionData: ConditionData = {
      conditionString: '',
      conditionParams: []
    };
    if (Array.isArray(filters) && filters.length === 1) {  // read description (declare) above to understand how to use it
      conditionData = this.setup_OR_condition(filters[0], this.alias, 1);
      if (conditionData.conditionString.length > 0) {
        condition = ' WHERE ( ' + conditionData.conditionString + ' ) ';
        params = conditionData.conditionParams;
      }
    }
    else {
      conditionData = this.setupConditionMap(filters, this.alias, 1);
      if (conditionData.conditionString.length > 0) {
        condition = ' WHERE ( ' + conditionData.conditionString + ' ) ';
        params = conditionData.conditionParams;
      }
    }

    const queryStatement = 'DELETE FROM ' + this.collection + condition;

    return new Promise<void>((resolve, reject) => {
      DbHelper.executeQuery(queryStatement, params, false).then(queryResult => {
        resolve();
      }).catch(err => {
        logger.log_error('ERROR deleteModels');
        // logger.log_c_error('ERROR deleteModels');
        reject(new Error('ERROR deleteModels'));
      });
    });
  }

  /**
   * batching insert models
   * @param sqlModels
   * @param columnArray
   * @returns {Promise<boolean>}
   */
  insertModels(sqlModels: any[], columnArray: string[]): Promise<void> {
    let valueParamStr = '';
    const valueArr: any[] = [];
    let i = 1;

    sqlModels.forEach(element => {
      valueParamStr += '(';
      for (const val of element) {
        valueParamStr += '$' + i + ',';
        valueArr.push(val);
        i++;
      }
      valueParamStr = valueParamStr.substr(0, valueParamStr.length - 1);
      valueParamStr += '),';
    });

    valueParamStr = valueParamStr.substr(0, valueParamStr.length - 1);
    const queryStatement = 'INSERT INTO ' + this.collection + '( ' + columnArray + ') ' + ' VALUES ' + valueParamStr;

    return new Promise<void>((resolve, reject) => {
      DbHelper.executeQuery(queryStatement, valueArr, false).then(queryResult => {
        resolve();
      }).catch(err => {
        logger.log_error('ERROR insertModels');
        // logger.log_c_error('ERROR insertModels');
        reject(new Error('ERROR insertModels'));
      });
    });
  }

  /**
   * @param key_valueMap
   * @param filters
   * @returns {Promise<boolean>}
   */
  updateModels(key_valueMap: any, filters: any): Promise<void> {
    const setCols: string[] = this.setupSetColumnParams(key_valueMap, 1);
    let queryStatement = 'UPDATE ' + this.collection + ' SET ' + setCols;
    let params = Object.keys(key_valueMap).map(function (key) {
      return key_valueMap[key]
    });
    const conditionData = this.setupConditionMap(filters, this.alias, params.length + 1);
    if (conditionData.conditionString.length > 0) {
      queryStatement += ' WHERE ( ' + conditionData.conditionString + ' ) ';
      params = params.concat(conditionData.conditionParams);
    }

    return new Promise<void>((resolve, reject) => {
      DbHelper.executeQuery(queryStatement, params, false).then(queryResult => {
        resolve();
      }).catch(err => {
        logger.log_error('ERROR updateModels');
        // logger.log_c_error('ERROR updateModels');
        reject(new Error('ERROR updateModels'));
      });
    });
  }

  /**
   * The INNER_JOIN (or JOIN) command between two tables/models
   * @param leftModel
   * @param rightModel
   * @param leftSelectionArr
   * @param rightSelectionArr
   * @param onCondition
   * @param leftFilters
   * @param rightFilters
   * @param limitations
   * @param isLimitationLeft
   * @param isQueryUnlimitedCount
   * @returns {Promise<QueryResult | any>}
   */
  join(leftModel: any, rightModel: any,
       leftSelectionArr: any[], rightSelectionArr: any[], onCondition: string,
       leftFilters: any, rightFilters = {}, limitations: DbLimitation, isLimitationLeft = true,
       isQueryUnlimitedCount = false): Promise<QueryResult | any> {
    if (!onCondition.toLowerCase().trim().startsWith('on ')) {
      onCondition = ' ON ' + onCondition;
    }

    let queryStatement = '';
    let selection = '';

    leftSelectionArr.forEach(elm => selection += (leftModel.alias + '.' + elm + ','));
    rightSelectionArr.forEach(elm => selection += (rightModel.alias + '.' + elm + ','));

    if (selection.length > 0) {
      selection = selection.substring(0, selection.lastIndexOf(','));
    }
    // setup condition
    let conditionData: ConditionData;

    if (Array.isArray(leftFilters) && leftFilters.length === 1) {  // read description (declare) above to understand how to use it
      conditionData = this.setup_OR_condition(leftFilters[0], leftModel.alias, 1);
    }
    else {
      conditionData = this.setupConditionMap(leftFilters, leftModel.alias, 1);
    }
    // console.log(chalk.blue('conditionData = '+JSON.stringify(conditionData)));
    let condition = conditionData.conditionString;
    let params = conditionData.conditionParams;

    conditionData = this.setupConditionMap(rightFilters, rightModel.alias, params.length + 1);
    if (conditionData.conditionString.length > 0) {
      if (condition.length > 0) {
        condition += ' AND ';
      }
      condition += conditionData.conditionString;
      params = params.concat(conditionData.conditionParams);
    }

    if (condition.length > 0) {
      condition = ' WHERE ( ' + condition + ' ) ';
    }

    const order_limit_conditionData: ConditionDataLimit =
      this.setupLimitations(isLimitationLeft ? this.alias : rightModel.alias,
        limitations, isQueryUnlimitedCount, params.length + 1);
    const orderCondition = order_limit_conditionData.orderCondition;
    const limitCondition = order_limit_conditionData.limitCondition;
    params = params.concat(order_limit_conditionData.conditionParams);

    const collectionLeft = leftModel.collectionName + ' as ' + leftModel.alias;
    const collectionRight = rightModel.collectionName + ' as ' + rightModel.alias;

    if (isQueryUnlimitedCount) {
      selection = ' COUNT(*) count ';
    }

    queryStatement = 'SELECT ' + selection
      + ' FROM ' + collectionLeft
      + ' JOIN ' + collectionRight
      + onCondition + condition
      + orderCondition + limitCondition;

    return new Promise((resolve, reject) => {
      DbHelper.executeQuery(queryStatement, params, isQueryUnlimitedCount).then(queryResult => {
        resolve(queryResult);
      }).catch(err => {
        logger.log_error('ERROR join');
        // logger.log_c_error('ERROR join');
        reject(new Error('ERROR join'));
      });
    });
  }

  /**
   * Setup condition string & its parameters for query
   * Cases:
   * filters is NULL --> no filter
   * filters === string --> return the string
   * filters === {} --> {key1: value1, key2: value2} <=> key1 LIKE value1 AND key2 LIKE value2 (key1 = value1 AND key2 = value2)
   *  {key: [value1, value2]} <=> key LIKE value1 OR key1 LIKE value2
   *  {key: {in: [value1, value2]} <=> key IN (value1, value2)
   *  {key: {anyKey: aNumber} <=> key anyKey aNumber
   * @param filters
   * @param alias
   * @param index
   * @returns {ConditionData}
   */
  public setupConditionMap(filters: any, alias: string, index: number): ConditionData {
    const result: ConditionData = {
      conditionString: '',
      conditionParams: []
    };
    if (!filters) {
      return result;
    } else if (typeof filters === 'string') {
      result.conditionString = filters;
    }
    else {
      let aliasStr = '';
      if (alias && alias.length > 0) {
        aliasStr = alias + '.';
      }
      let value: any;
      Object.keys(filters).forEach(key => {
        value = filters[key];
        if (typeof value === 'string' && value.toString().length > 0) {
          if (value.indexOf('%') > -1) {
            result.conditionString += ' AND LOWER(' + aliasStr + key + ') LIKE LOWER($' + index + ') ';
          }
          else {
            result.conditionString += ' AND ' + aliasStr + key + ' LIKE $' + index;
          }
          result.conditionParams.push(value);
          index++;
        }
        else if (typeof value === 'number') {
          result.conditionString += ' AND ' + aliasStr + key + ' = $' + index;
          result.conditionParams.push(value);
          index++;
        }
        else if (Array.isArray(value)) {
          let or_condition = '';
          for (const elem of value) {
            if (typeof elem === 'string' && elem.toString().length > 0) {
              if (key.indexOf('%') > -1) {
                or_condition += ' OR LOWER(' + aliasStr + elem + ') LIKE LOWER($' + index + ') ';
              }
              else {
                or_condition += ' OR ' + aliasStr + elem + ' LIKE $' + index;
              }
            }
            else if (typeof value === 'number') {
              or_condition += ' OR ' + aliasStr + elem + ' = $' + index;
            }
            else {
              continue;
            }
            result.conditionParams.push(key);
            index++;
          }
          or_condition = or_condition.substr(or_condition.indexOf('OR ') + 2).trim();
          or_condition = ' ( ' + or_condition + ' ) ';
          result.conditionString += ' AND ' + or_condition;
        } else if (typeof value === 'object') {  // case value is object like: key: {in:[a,b,c,...]}, key: {'<':8}
          Object.keys(value).forEach(key_v => {
            const value_v = value[key_v];
            // firstly, convert the predefined key (IN/NOT IN) if needed
            key_v = DbPostgres.getConditionKey(key_v.trim().toLowerCase());
            if (Array.isArray(value_v) && value_v.length > 0) { // case condition is 'key IN/NOT IN (a,b,c,...)'
              let customCondition = aliasStr + key + ` ${key_v} (`;
              value_v.forEach(elem => {
                customCondition += '$' + index + ',';
                result.conditionParams.push(elem);
                index++;
              });
              customCondition = customCondition.substring(0, customCondition.lastIndexOf(','));
              customCondition = customCondition + ')';
              customCondition = ' ( ' + customCondition + ' ) ';
              result.conditionString += ' AND ' + customCondition;
            }
            else if (typeof value_v === 'string' || typeof value_v === 'number') {
              result.conditionString += ' AND ' + aliasStr + key + ' ' + key_v + ' $' + index;
              result.conditionParams.push(value_v);
              index++;
            }
          });
        }
      });
      if (result.conditionString.length > 0) {
        result.conditionString = result.conditionString.substr(result.conditionString.indexOf('AND ') + 3).trim();
      }
    }

    // console.log(chalk.green('setupConditionMap = '+JSON.stringify(result)));
    return result;
  }

  /**
   * setup OR condition string & its parameters for query
   * @param filters
   * @param alias
   * @param index
   * @returns {ConditionData}
   */
  private setup_OR_condition(filters: any, alias: string, index: number): ConditionData {
    const result: ConditionData = {
      conditionString: '',
      conditionParams: []
    };
    let aliasStr = '';
    if (alias && alias.length > 0) {
      aliasStr = alias + '.';
    }
    let value: any;
    Object.keys(filters).forEach(key => {
      value = filters[key];
      if (typeof value === 'number') {
        result.conditionString += ' OR ' + aliasStr + key + ' = $' + index;
      }
      else {
        if (value.indexOf('%') > -1) {
          result.conditionString += ' OR LOWER(' + aliasStr + key + ') LIKE LOWER($' + index + ') ';
        }
        else {
          result.conditionString += ' OR ' + aliasStr + key + ' LIKE $' + index;
        }
      }
      index++;
      result.conditionParams.push(value);
    });
    if (result.conditionString.length > 0) {
      result.conditionString = result.conditionString.substr(result.conditionString.indexOf('OR ') + 2).trim();
    }
    // console.log(chalk.green('setup_OR_condition = '+JSON.stringify(result)));
    return result;
  }

  /**
   * Setup the order & limit condition for the query
   * @param alias
   * @param limitations
   * @param isQueryUnlimitedCount
   * @param index
   * @returns {ConditionDataLimit}
   */
  private setupLimitations(alias: string, limitations: DbLimitation,
                           isQueryUnlimitedCount: boolean = false, index: number): ConditionDataLimit {
    const result: ConditionDataLimit = {
      orderCondition: '',
      limitCondition: '',
      conditionParams: []
    };

    if (!isQueryUnlimitedCount && limitations) {
      if (limitations.sorter && limitations.sorter.length > 0) {
        result.orderCondition = ' ORDER BY ' + alias + '.' + limitations.sorter + ' ';
        if (limitations.direct && limitations.direct.length > 0) {
          result.orderCondition += ' ' + limitations.direct
        }
      }
      if (limitations.offset > -1 && limitations.limit > 0) {
        result.limitCondition = ' LIMIT $' + index;
        result.conditionParams.push(limitations.limit);
        index++;
        result.limitCondition += ' OFFSET $' + index;
        result.conditionParams.push(limitations.offset);
      }
    }
    return result;
  }

  /**
   * @param key_valueMap
   * @param index
   * @returns {string[]}
   */
  private setupSetColumnParams(key_valueMap: any, index: number): string[] {
    const key_value_arr: string[] = [];
    Object.keys(key_valueMap).forEach(key => {
      key_value_arr.push(key + ' = $' + index + '');
      index++;
    });
    return key_value_arr;
  }

  /**
   * @param key_valueMap
   * @param index
   * @returns {ParamsValue}
   */
  private setupObjValueParams(key_valueMap: any, index: number): ParamsValue {
    const valueObj: ParamsValue = {
      valueParams: [],
      values: []
    };
    Object.keys(key_valueMap).forEach(key => {
      valueObj.valueParams.push('$' + index);
      valueObj.values.push(key_valueMap[key]);
      index++;
    });
    return valueObj;
  }
}

class ConditionData {
  conditionString: string;
  conditionParams: any[];
}

class ConditionDataLimit {
  orderCondition: string;
  limitCondition: string;
  conditionParams: any[];
}

class ParamsValue {
  valueParams: string[];
  values: string[];
}

class QueryResult {
  models: any[];
  count_total_unlimited: number;

  constructor() {
    this.models = [];
    this.count_total_unlimited = 0;
  }
}

export {DbHelper, QueryResult};
