import {Request, Response, Router} from 'express';
import * as _ from 'lodash';

import {Logger} from '../services/Logger';
import {Game} from '../models/game/Game';
import {ERROR} from '../ErrorDefinition';
import {DbLimitation} from '../models/_modelHelper/DbLimitation';
import {ModelHeper} from '../services/ModelHeper';
import {User} from '../models/user/User';
import {Match} from '../models/game/Match';
import {MatchRecord} from '../models/game/MatchRecord';
import {GameInfo, RecordInfo, UserGameInfo} from '../models/user/UserGameInfo';

const logger = new Logger('ROUTER_GAME');

const game = new Game();
const user = new User();
const match = new Match();
const matchRecord = new MatchRecord();
const userGameInfo = new UserGameInfo('');

const router_game: Router = Router();

router_game.get('/', async (req: Request, res: Response) => {
  try {
    const models = await game.findModels(new DbLimitation(), {});

    return res.json({
      data: models.map((m) => m.getKeyValueObj()),
    });
  }
  catch (err) {
    res.status(500).json(ERROR.code500);
  }
});

router_game.post('/', async ({ body }: Request, res: Response) => {
  try {
    const model = new Game();
    ModelHeper.setupModel(model, {value: {...body}}, false);

    const foundModel = await model.findByKeyidOrCode(model.keyid, model.value.code);
    if (foundModel === null) {
      await model.insert();
      return res.status(200).json(model.getKeyValueObj());
    }
    else {
      res.status(409).json(ERROR.code409);
    }
  }
  catch (err) {
    logger.log_c_debug('err = ', err);
    res.status(500).json(ERROR.code500);
  }
});

/**
 * Create a new Match for a Game
 */
interface Participant {
  user_keyid: string;
  status: 0 | 1;
}
const isUserWin = (status: number) => status === 1;
router_game.post('/:game_id/match', async ({ params, body }: Request, res: Response) => {
  try {
    const {game_id} = params;
    const participants: Array<Participant> = body.participants;
    if (participants.length === 0) {
      throw new Error('No participants');
    }

    const user_keyids = participants.map((p) => p.user_keyid);

    const foundGame = await game.findByKeyidOrCode(game_id);
    const foundUsers = await user_keyids.map(async (keyid) => await user.findByKeyidOrUsername(keyid));
    if (foundGame == null || foundUsers.filter((u) => u === null).length > 0) {
      throw new Error('Game Id or Participant Id is incorrect');
    }

    const matchModel = new Match();
    matchModel.value.game_keyid = game_id;
    matchModel.value.participant = user_keyids.length;
    await matchModel.insert();

    await user_keyids.forEach(async (user_keyid) => {
      const matchRecordModel = new MatchRecord();
      matchRecordModel.value.match_keyid = matchModel.keyid;
      matchRecordModel.value.user_keyid = user_keyid;
      matchRecordModel.value.status = participants.find((p) => p.user_keyid === user_keyid).status;
      await matchRecordModel.insert();

      /**
       * Update User Game Information, inferred from MatchRecord
       * Async, don't need to care the result of the process
       */
      const recordValue = matchRecordModel.value;
      /**
       * Todo: Optimization
       * 1. we may using JOIN in query to reduce the step of processing below
       * 2. we may calculate accumulate for each new record instead of re-calculate all like below
       */
      const matchRecordsOfUser = await matchRecord.findModels(new DbLimitation(), {user_keyid: recordValue.user_keyid});
      const matchKeyids = matchRecordsOfUser.map((m) => m.value.match_keyid);
      const matchesOfUser = await match.findModels(new DbLimitation(), {keyid: {in: matchKeyids}});
      const allParticipantMatchRecords = await matchRecord.findModels(new DbLimitation(), {match_keyid: {in: matchKeyids}});
      const gameKeyids = _.uniq(matchesOfUser.map((m) => m.value.game_keyid));

      // logger.log_info('matchRecordsOfUser = ', matchRecordsOfUser);
      // logger.log_info('matchKeyids = ', matchKeyids);
      // logger.log_info('matchesOfUser = ', matchesOfUser);
      // logger.log_info('allParticipantMatchRecords = ', allParticipantMatchRecords);
      // logger.log_info('gameKeyids = ', gameKeyids);

      const gameInfo: GameInfo = {};
      gameKeyids.forEach((game_keyid) => {
        const gameMatchesOfUser = matchesOfUser.filter((m) => m.value.game_keyid === game_keyid);
        const gameMatchKeyids = gameMatchesOfUser.map((m) => m.keyid);
        const gameMatchRecordsOfUser = matchRecordsOfUser
          .filter((r) => gameMatchKeyids.indexOf(r.value.match_keyid) > -1);
        const gameOpponentMatchRecords = allParticipantMatchRecords.filter((r) => {
          return gameMatchKeyids.indexOf(r.value.match_keyid) > -1 && r.value.user_keyid !== recordValue.user_keyid;
        });
        // logger.log_info('gameMatchesOfUser = ', gameMatchesOfUser);
        // logger.log_info('gameMatchKeyids = ', gameMatchKeyids);
        // logger.log_info('gameMatchRecordsOfUser = ', gameMatchRecordsOfUser);
        // logger.log_info('gameOpponentMatchRecords = ', gameOpponentMatchRecords);
        const records: RecordInfo = {};
        _.uniq(gameOpponentMatchRecords.map((opp) => opp.value.user_keyid)).forEach((opponent_keyid) => {
          records[opponent_keyid] = {
            total: 0,
            win: 0,
          };
        });
        gameMatchKeyids.forEach((match_keyid) => {
          const isWin = isUserWin(
            gameMatchRecordsOfUser
              .find((r) => r.value.match_keyid === match_keyid).value.status
          );
          gameOpponentMatchRecords
            .filter((r) => r.value.match_keyid === match_keyid)
            .forEach((r) => {
              const isSameTeam = isUserWin(r.value.status) === isWin;
              if (!isSameTeam) {
                records[r.value.user_keyid].total = records[r.value.user_keyid].total + 1;
                records[r.value.user_keyid].win = records[r.value.user_keyid].win + (isWin ? 1 : 0);
              }
            });
        });

        gameInfo[game_keyid] = {
          total: gameMatchesOfUser.length,
          win: gameMatchRecordsOfUser.filter((r) => r.value.status === 1).length,
          records,
        }
      });

      let uGIModel = await userGameInfo.findByKeyid(recordValue.user_keyid);
      if (uGIModel === null) {
        uGIModel = new UserGameInfo(recordValue.user_keyid);
        uGIModel.value.game_info = gameInfo;
        uGIModel.insert();
      } else {
        uGIModel.value.game_info = gameInfo;
        uGIModel.save();
      }
    });

    return res.status(200).json(matchModel.getKeyValueObj());
  }
  catch (err) {
    res.status(500).json(err);
  }
});

/**
 * Delete a Match
 * Todo: delete a match --> update game_info of users
 */
router_game.delete('/match/:match_keyid', async ({ params }: Request, res: Response) => {
  try {
    const {match_keyid} = params;
    const foundMatch = await match.findByKeyid(match_keyid);
    if (foundMatch === null) {
      return res.status(404).json(ERROR.code404)
    }

    const foundRecords = await matchRecord.findModels(new DbLimitation(), {match_keyid});
    await foundRecords.forEach(async (r) => await r.deleteByKeyId());
    await foundMatch.deleteByKeyId();
    return res.status(200).send(`Deleted match = ${match_keyid}`);
  }
  catch (err) {
    res.status(500).json(ERROR.code500);
  }
});

export default router_game;
