import {Request, Response, Router} from 'express';
import * as _ from 'lodash';


import {Logger} from '../services/Logger';
import {DbLimitation} from '../models/_modelHelper/DbLimitation';
import {ERROR} from '../ErrorDefinition';
import {ModelHeper} from '../services/ModelHeper';
import {User} from '../models/user/User';
import {GameInfo, UserGameInfo} from '../models/user/UserGameInfo';
import {Game} from '../models/game/Game';

const logger = new Logger('API_User');

const user = new User();
const game = new Game();
const userGameInfo = new UserGameInfo('');

const router_user: Router = Router();

router_user.get('/', async (req: Request, res: Response) => {
  try {
    const models = await user.findModels(new DbLimitation(), {});

    return res.json({
      data: models.map((m) => m.getKeyValueObj()),
    });
  }
  catch (err) {
    res.status(500).json(ERROR.code500);
  }
});

router_user.post('/', async ({ body }: Request, res: Response) => {
  try {
    const model = new User();
    ModelHeper.setupModel(model, {value: {...body}}, false);

    const foundModel = await model.findByKeyidOrUsername(model.keyid, model.value.username);
    logger.log_c_debug('found User = ', foundModel);
    if (foundModel === null) {
      await model.insert();
      return res.status(200).json(model.getKeyValueObj());
    }
    else {
      res.status(409).json(ERROR.code409);
    }
  }
  catch (err) {
    logger.log_c_debug('err = ', err);
    res.status(500).json(ERROR.code500);
  }
});

router_user.get('/:user_keyid', async ({params}: Request, res: Response) => {
  try {
    const model = await userGameInfo.findByKeyid(params.user_keyid);
    if (model === null) {
      return res.status(404).json(ERROR.code404);
    }

    const game_info: GameInfo = model.value.game_info;
    const gameKeyids: Array<string> = Object.keys(game_info);
    let participantKeyids: Array<string> = [];
    gameKeyids
      .map((gKeyid) => game_info[gKeyid])
      .forEach(({records}) => {
        participantKeyids = _.union(participantKeyids, Object.keys(records));
    });

    const games = await game.findModels(new DbLimitation(), {keyid: {in: gameKeyids}});
    const users = await user.findModels(new DbLimitation(), {keyid: {in: participantKeyids}});

    if (games.length === 0 || users.length === 0) {
      return res.status(404).json(ERROR.code404);
    }

    await gameKeyids.forEach(async (gKeyid) => {
      const gameInfo = await games.find((g) => g.keyid === gKeyid);
      if (gameInfo !== null) {
        game_info[gKeyid]['game_code'] = gameInfo.value.code;
        game_info[gKeyid]['game_name'] = gameInfo.value.name;
        game_info[gKeyid]['game_img'] = gameInfo.value.img_url;
      }
      Object.keys(game_info[gKeyid].records).forEach((pKey) => {
        const participant = users.find((p) => p.keyid === pKey)
        game_info[gKeyid].records[pKey]['username'] = participant.value.username;
        game_info[gKeyid].records[pKey]['display_name'] = participant.value.display_name;
        game_info[gKeyid].records[pKey]['img_url'] = participant.value.img_url;
      })
    });

    return res.json({
      data: game_info,
    });
  }
  catch (err) {
    res.status(500).json(ERROR.code500);
  }
});

export default router_user;
