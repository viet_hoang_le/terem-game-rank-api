import router_game from './game';
import router_user from './user';

export {
  router_game,
  router_user,
};
