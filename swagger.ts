export default {
  'swagger': '2.0',
  'info': {
    'version': '1.0.0',
    'title': 'Node.js Game Ranking System API',
    'description': 'Terem Full-stack Challenge',
    'license': {
      'name': 'MIT',
      'url': 'https://opensource.org/licenses/MIT'
    }
  },
  'host': 'ec2-13-125-251-167.ap-northeast-2.compute.amazonaws.com:3008',
  'basePath': '/',
  'tags': [
    {
      'name': 'Game Ranking System',
      'description': 'API for Game Ranking System'
    }
  ],
  'schemes': [
    'http'
  ],
  'consumes': [
    'application/json'
  ],
  'produces': [
    'application/json'
  ],
  'paths': {
    '/game': {
      'post': {
        'tags': [
          'Game'
        ],
        'description': 'Create a new Game',
        'parameters': [
          {
            'name': 'body',
            'in': 'body',
            'type': 'string',
            'required': true,
            'description': 'Parameter to create Game',
            'schema': {
              '$ref': '#/definitions/CreateGameParams'
            }
          },
        ],
        'produces': [
          'application/json'
        ],
        'responses': {
          '200': {
            'description': 'New game is created',
            'schema': {
              '$ref': '#/definitions/Game'
            }
          }
        }
      },
      'get': {
        'tags': [
          'Games'
        ],
        'summary': 'Get all games in system',
        'responses': {
          '200': {
            'description': 'OK',
            'schema': {
              '$ref': '#/definitions/Games'
            }
          }
        }
      }
    },
    '/user': {
      'post': {
        'tags': [
          'User'
        ],
        'description': 'Create a new User',
        'parameters': [
          {
            'name': 'body',
            'in': 'body',
            'type': 'string',
            'required': true,
            'description': 'Create a new User with a unique username',
            'schema': {
              '$ref': '#/definitions/CreateUserParams'
            }
          },
        ],
        'produces': [
          'application/json'
        ],
        'responses': {
          '200': {
            'description': 'New User is created',
            'schema': {
              '$ref': '#/definitions/User'
            }
          }
        }
      },
      'get': {
        'tags': [
          'Users'
        ],
        'summary': 'Get all users in system',
        'responses': {
          '200': {
            'description': 'OK',
            'schema': {
              '$ref': '#/definitions/Users'
            }
          }
        }
      }
    },
    '/user/{user_keyid}': {
      'parameters': [
        {
          'name': 'user_keyid',
          'in': 'path',
          'required': true,
          'description': 'Id of a user',
          'type': 'string'
        }
      ],
      'get': {
        'tags': [
          'User Game Info'
        ],
        'summary': 'Retrieve Game Info of a User',
        'responses': {
          '200': {
            'description': 'OK',
            'schema': {
              '$ref': '#/definitions/UserGameInfo'
            }
          }
        }
      }
    },
    '/game/{game_id}/match': {
      'parameters': [
        {
          'name': 'game_id',
          'in': 'path',
          'required': true,
          'description': 'Id of the game',
          'type': 'string'
        }
      ],
      'post': {
        'tags': [
          'Match'
        ],
        'description': 'Add a new Match',
        'parameters': [
          {
            'name': 'body',
            'in': 'body',
            'required': true,
            'description': 'Create a new Match of participants of a Game',
            'schema': {
              'properties': {
                'participants': {
                  'type': 'array',
                  'items': {
                    '$ref': '#/definitions/CreateGameMatchParams'
                  },
                },
              },
            },
          },
        ],
        'produces': [
          'application/json'
        ],
        'responses': {
          '200': {
            'description': 'New Game Match is created',
          }
        }
      },
    },
    '/game/match/{match_keyid}': {
      'parameters': [
        {
          'name': 'match_keyid',
          'in': 'path',
          'required': true,
          'description': 'Id of the Match',
          'type': 'string'
        }
      ],
      'delete': {
        'tags': [
          'Match'
        ],
        'description': 'Delete a Match by its Id',
        'responses': {
          '200': {
            'description': 'Deleted'
          },
          '404': {
            'description': 'Not Found'
          },
          '500': {
            'description': 'Internal Error'
          }
        }
      },
    },
  },
  'definitions': {
    'CreateGameParams': {
      'required': [
        'code',
        'name',
      ],
      'properties': {
        'code': {
          'type': 'string',
          'minLength': 3,
          'maxLength': 14,
          'uniqueItems': true
        },
        'name': {
          'type': 'string',
          'maxLength': 128,
        },
        'description': {
          'type': 'string',
          'maxLength': 512,
        },
        'img_url': {
          'type': 'string',
          'maxLength': 1024,
        }
      }
    },
    'CreateUserParams': {
      'required': [
        'username',
      ],
      'properties': {
        'username': {
          'type': 'string',
          'minLength': 3,
          'maxLength': 20,
          'uniqueItems': true
        },
        'display_name': {
          'type': 'string',
          'maxLength': 50,
        },
        'img_url': {
          'type': 'string',
          'maxLength': 1024,
        },
      }
    },
    'CreateGameMatchParams': {
      'required': [
        'user_keyid',
        'status',
      ],
      'properties': {
        'user_keyid': {
          'type': 'string',
          'minLength': 20,
          'maxLength': 62,
        },
        'status': {
          'type': 'number',
          'enums': [0, 1],
        },
      }
    },
    'User': {
      'required': [
        'keyid',
        'username',
        'display_name',
      ],
      'properties': {
        'keyid': {
          'type': 'string',
          'uniqueItems': true
        },
        'username': {
          'type': 'string',
          'uniqueItems': true
        },
        'display_name': {
          'type': 'string'
        },
        'img_url': {
          'type': 'string'
        },
        'updated_at': {
          'type': 'number'
        },
        'created_at': {
          'type': 'number'
        }
      }
    },
    'Users': {
      'type': 'array',
      '$ref': '#/definitions/User'
    },
    'UserGameInfo': {
      'required': [
        'keyid',
      ],
      'properties': {
        'game_info': {
          'type': 'object',
        },
      }
    },
    'Game': {
      'required': [
        'code',
        'name',
        'description',
        'img_url',
      ],
      'properties': {
        'keyid': {
          'type': 'string',
          'uniqueItems': true
        },
        'code': {
          'type': 'string',
          'uniqueItems': true
        },
        'name': {
          'type': 'string'
        },
        'description': {
          'type': 'string'
        },
        'img_url': {
          'type': 'string'
        },
        'updated_at': {
          'type': 'number'
        },
        'created_at': {
          'type': 'number'
        }
      }
    },
    'Games': {
      'type': 'array',
      '$ref': '#/definitions/Game'
    }
  }
};
