import * as express from 'express';
import {join} from 'path';
import * as logger from 'morgan';
import * as favicon from 'serve-favicon';
import {json, urlencoded} from 'body-parser';
import * as session from 'express-session';
import * as cors from 'cors';
import {debug} from 'debug';

import {CORS} from './app_config';

import {router_index} from './routes';
import {
  router_game,
  router_user,
} from './routes/api';

import * as swaggerUi from 'swagger-ui-express';
import swaggerDocument from './swagger';

const app: express.Application = express();

// use cors to accept Cross-origin
const corsOptions = {
  origin: function (origin, callback) {
    // checking if the origin is in Whitelist of access
    let isValid = CORS.isTesting;
    if (!CORS.isTesting && origin) {
      for (const item of CORS.WHITELIST) {
        isValid = origin.indexOf(item) > -1 && origin.indexOf(item) < 7;
        if (isValid) {
          break;
        }
      }
    }
    // debug('app')('origin = ' + origin);
    // debug('app')('Accept = ' + isValid);
    if (isValid) {
      callback(null, true)
    }
    else {
      callback(new Error('Not allowed by CORS'))
    }
  }
};
app.use(cors(corsOptions));

// view engine setup
app.set('views', join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(favicon(join(__dirname, 'public/images', 'favicon.ico')));

app.use(session({secret: 'bibooki-session-secret-key', saveUninitialized: true, resave: true}));

app.use(function (req: express.Request, res: express.Response, next: express.NextFunction) {
  req.session['locale'] = 'en';
  res.locals.session = req.session;
  next();
});

app.use(logger('dev'));
app.use(json({limit: '5mb'}));
app.use(urlencoded({extended: false}));
app.use(express.static(join(__dirname, 'public')));

app.use('/', router_index);
app.use('/game', router_game);
app.use('/user', router_user);

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  const err = new Error('Not Found');
  // err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function (err: any, req: express.Request, res: express.Response, next: express.NextFunction) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err: any, req: express.Request, res: express.Response, next: express.NextFunction) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;
